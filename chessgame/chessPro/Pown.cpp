#include "Pown.h"

bool Pown::check(std::string movement)
{
	char x = movement[0];
	char y = movement[1];
	char myStart = _color ? 'a' : 'A';
	char myEnd = _color ? 'z' : 'Z';
	char otherStart = _color ? 'A' : 'a';
	char otherEnd = _color ? 'Z' : 'z';
	int direction = _color ? -1 : 1;
	if(x == _x && (y == _y + direction || (_y == (_color ? '7' : '2') && y == (_color ? '5': '4'))) && _board.isEmpty(_board.getIndex(movement)))
		return true;
	if ((x == _x + 1 || x == _x - 1) && y == _y +direction && !_board.isEmpty(_board.getIndex(movement)))
		return true;
	return false;
}
