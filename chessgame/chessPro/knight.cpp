#include "knight.h"

#define BOARD_SIZE 64
/* function will check if the solider can walk this.
input: string movement - place 0 - x, place 1 - y.
output: true - the movement is ok
false - Invalid tool displacement*/
bool Knight::check(std::string movement)
{
	char newY = movement[1];
	char newX = movement[0];
	int disX = newX - _x;
	int disY = newY - _y; 
	/*A horseman can move two steps forward / back and one step aside.
	so the distance in the x axis will always be 1 or -1 and the y axis 2 or -2. 
	Or two steps sideways and one step forward / backward. 
	so the distance in the x axis will always be 2 or -2 and the y axis 1 or -1. 
	So the distance between the starting point and destination on the x axis
	mul the distance between the starting point and the destination on the y axis 
	will always be 2 or -2.*/
	if (disX*disY == 2 || disX*disY == -2) 
	{
		return true;
	}
	else
	{
		return false;
	}
}