
#pragma once
#include "Soliders.h"

class King : public Solider
{
public:
	King(Board& board, char x, char y, bool color) : Solider(board, x, y, color) {};
	virtual bool check(std::string movement);
};