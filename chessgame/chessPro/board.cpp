
#include "board.h"
#include "rook.h"
#include "King.h"
#include "knight.h"
#include "Pown.h"
#include "bishop.h"
#include "queen.h"
/*
checking if a point in the board is empty
input: index - the index in the string board
output: if the location is empty
*/
bool Board::isEmpty(int index)
{
	return _board[index] == '#';
}

/*
getting the string location(like A4 or G2) and find the index in the table string
input: loc - the string location
output: the index
*/
int Board::getIndex(std::string loc)
{
	int a1 = (loc[0] - 'A');
	int a2 = ('8' - loc[1]);
	return (a2 * 8 + a1);
}

/*
getting the index in the table string  and find the string location(like A4 or G2)
input: index - the index in the table string
output: the string location
*/
std::string Board::getLoc(char index)
{
	std::string temp = "";
	char x = 'A' + (index % 8);
	char y = '8' - (index / 8);
	temp.append({ x, y });
	return temp;
}

/*
this function check if the src point is good
input: the src location in string
output: if the source location is ok
*/
bool Board::checkSrc(std::string src)
{
	return  (_turn ? 'a' : 'A') <= _board[getIndex(src)] && _board[getIndex(src)] <= (_turn ? 'z' : 'Z');
}

/*
this function check if the dst point is good
input: the dst location in string
output: if the destination location is ok
*/
bool Board::checkDst(std::string dst)
{
	return !((_turn ? 'a' : 'A') <= _board[getIndex(dst)] && _board[getIndex(dst)] <= (_turn ? 'z' : 'Z')) || _board[getIndex(dst)] == '#';
}

/*this function check if the board in a chess
output: 0 - nothing chess, 1 - if only white chess, 2 - if only black chess, and 3 - if both
*/
int Board::checkChess()
{
	int whiteKing = 0;
	int blackKing = 0;
	bool blackChess = false;
	bool whiteChess = false;
	for (char i = '1'; i <= '8'; i++)
	{
		for (char j = 'A'; j < 'I'; j++)
		{
			std::string temp = "";
			temp.append({ j,i });
			int loc = getIndex(temp);
			if (_board[loc] == 'K')
				whiteKing = loc;
			if (_board[loc] == 'k')
				blackKing = loc;
		}
	}
	for (char i = '1'; i <= '8'; i++)
	{
		for (char j = 'A'; j < 'I'; j++)
		{
			std::string temp = "";
			temp.append({ j,i });
			int index = getIndex(temp);
			if (_board[index] == '#')
				continue;
			Solider* sol = getSoliderByLetter(_board[index], j, i);
			if (sol && !sol->_color)
			{
				whiteChess = sol->check(getLoc(blackKing)) ? true : whiteChess;
			}
			if (sol && sol->_color)
			{
				blackChess = _board[index] != 'k' && sol->check(getLoc(whiteKing)) ? true : blackChess;
			}
			delete sol;
		}
	}
	return whiteChess + 2 * blackChess;
}

/*cheking a chess after a step
input: step - the step to check
output: 0 - nothing chess, 1 - if only white chess, 2 - if only black chess, and 3 - if both
*/
int Board::checkChess(std::string step)
{
	std::string temp = _board;
	std::string src = "";
	std::string dst = "";
	src.append({ step[0], step[1] });
	dst.append({ step[2], step[3] });
	apply(src, dst);
	int ret = checkChess();
	_board = temp;
	return ret;
}


/*
finding a solider for a spesific place and type of solider
input: c- the letter of type of the solider. x and y - the location of the solider
output: a pointer to solider in the correct type
*/
Solider* Board::getSoliderByLetter(char c, char x, char y)
{
	switch (c)
	{
	case 'r':
		return new Rook(*this, x, y, true);
		break;
	case 'R':
		return new Rook(*this, x, y, false);
		break;
	case 'k':
		return new King(*this, x, y, true);
		break;
	case 'K':
		return new King(*this, x, y, false);
		break;
	case 'p':
		return new Pown(*this, x, y, true);
		break;
	case 'P':
		return new Pown(*this, x, y, false);
		break;
	case 'n':
		return new Knight(*this, x, y, true);
		break;
	case 'N':
		return new Knight(*this, x, y, false);
		break;
	case 'b':
		return new Bishop(*this, x, y, true);
		break;
	case 'B':
		return new Bishop(*this, x, y, false);
		break;
	case 'q':
		return new Queen(*this, x, y, true);
		break;
	case 'Q':
		return new Queen(*this, x, y, false);
		break;
	}
	return nullptr;
}

/*checking if the index of the step is legall
input: step - the step to check
output: if the indexes is legall
*/
bool Board::checkIndex(std::string step)
{
	return 'A' <= step[0] && step[0] <= 'H' && 'A' <= step[2] && step[2] <= 'H' && '1' <= step[1] && step[1] <= '8' && '1' <= step[3] && step[3] <= '8';
}

/*checking that the src and dst is not same place
input: the step to check
output: if the indexes is diffrent
*/
bool Board::checkSame(std::string step)
{
	return !((step[0] == step[2]) && (step[1] == step[3]));
}

/*getting a code result and update the board
input: step - the step of the turn
output: the code result
*/
int Board::getCode(std::string step)
{
	std::string base = "";
	base.push_back(step[0]);
	base.push_back(step[1]);
	std::string dst = "";
	dst.push_back(step[2]);
	dst.push_back(step[3]);
	int baseIndex = getIndex(base);
	if (!checkSrc(base))
		return 2;
	if (!checkDst(dst))
		return 3;
	if (!checkIndex(step))
		return 5;
	if (!checkSame(step))
		return 7;
	int chess = checkChess(step);
	bool my = !_turn ? chess > 1 : chess % 2;
	bool other = _turn ? chess > 1 : chess % 2;
	if (my)
		return 4;
	Solider* baseSolider = getSoliderByLetter(_board[baseIndex], base[0], base[1]);
	if (!baseSolider->check(dst))
	{
		delete baseSolider;
		return 6;
	}
	_turn = !_turn;
	apply(base, dst);
	if (other)
		return 1;
	return 0;
}

//printing the board
void Board::print()
{
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
			std::cout << _board[(i * 8) + j] << ' ';
		std::cout << std::endl;
	}
}

/*appling a step on the board
input: src and dst - a string loction for the step
*/
void Board::apply(std::string src, std::string dst)
{
	_board[getIndex(dst)] = _board[getIndex(src)];
	_board[getIndex(src)] = '#';
}