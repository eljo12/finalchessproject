#pragma once
#include "soliders.h"
#include "rook.h"
#include "bishop.h"
class Queen : public Solider
{
public:
	Queen(Board& board, char x, char y, bool color) : Solider(board, x, y, color) {};
	virtual bool check(std::string movement) override;
};