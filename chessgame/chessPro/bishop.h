#pragma once
#include "soliders.h"

class Bishop : public Solider
{
public:
	Bishop(Board& board, char x, char y, bool color) : Solider(board, x, y, color) {};
	virtual bool checkEmpyWay(char dstX, char dstY);
	virtual bool check(std::string movement);
};