#include "bishop.h"

#define BOARD_SIZE 64


/* function will check if the way is empty
output: 0 - way is empty
1 - somthing worng*/
bool Bishop::checkEmpyWay(char dstX, char dstY)
{
	char directionX = dstX - _x > 0 ? 1 : -1;
	char directionY = dstY - _y > 0 ? -1 : 1;
	for (char i = 8 * ('8' - _y) + _x - 'A'; i != 8 * ('8' - dstY) + dstX - 'A'; i += 8 * directionY + directionX)
	{
		char j = (8 * ('8' - _y) + _x - 'A');
		if (!_board.isEmpty(i))
			if (i != j)
				return false;
	}
		return true;
}


/* function will check if the solider can walk this.
input: string movement - place 0 - x, place 1 - y.
output: 0 - the movement is ok
6 - Invalid tool displacement*/
bool Bishop::check(std::string movement)
{
	return ((_x - movement[0] == _y - movement[1] || _x - movement[0] == movement[1] - _y) && checkEmpyWay(movement[0], movement[1]));
}

