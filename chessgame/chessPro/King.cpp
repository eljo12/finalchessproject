
#include "King.h"

#define BOARD_SIZE 64
/* function will check if the solider can walk this.
input: string movement - place 0 - x, place 1 - y.
output: 0 - the movement is ok
6 - Invalid tool displacement*/
bool King::check(std::string movement)
{
	char newY = movement[1];
	char newX = movement[0];
	int x = (int)newX;
	int y = (int)newY;

	if ((x == (int)_x || x == (int)_x - 1 || x == (int)_x + 1) && y == (int)_y || y == (int)_y - 1 || y == (int)_y + 1)
	{
		return true;
	}
	else
	{
		return false;
	}
}