
#pragma once
#include <iostream>
#include "board.h"

class Board;
class Solider
{
public:
	virtual bool check(std::string movement) = 0;
	Solider(Board& board, char x, char y, bool color) : _board(board), _x(x), _y(y), _color(color) {};
	char _x;
	char _y;
	bool _color;
	Board& _board;

};

