
#pragma once
#include "soliders.h"
#include <iostream>
class Solider;
class Board
{
public:
	Board(std::string start) : _turn(start[64] != '0'), _board(start) {}
	bool isEmpty(int index);
	int getIndex(std::string loc);
	std::string getLoc(char index);
	void print();
	int getCode(std::string step);
private:
	std::string _board;
	bool _turn;


	bool checkSrc(std::string src);
	bool checkDst(std::string step);
	int checkChess();
	int checkChess(std::string step);
	Solider* getSoliderByLetter(char c, char x, char y);
	bool checkIndex(std::string step);
	bool checkSame(std::string step);
	void apply(std::string src, std::string dst);

};